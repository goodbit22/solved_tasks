"""
Wywołaj funkcję map() wraz z dwoma argumentami:
a) funkcją lambda, która podniesie do potęgi drugiej liczbę podaną jako jej argument;
b) listę zawierającą liczby parzyste z zakresu od 4 do 15; listę wygeneruj za pomocą funkcji
typu inline i wykorzystaj w niej funkcję range().
"""
w = lambda x: x * x
print(list(map(lambda x: x *x , [2] )))

lista = list(range(20))
print( list(filter(lambda x: x % 2 == 0 , list(filter( lambda x: x >= 4  and x <= 15, lista)) )))
