"""
Użyj listy składanej do wyświetlenia liczb od 1 do 30 włącznie, które są podzielne przez 3
"""
print([ x for x in range(1,31) if x % 3 == 0])
