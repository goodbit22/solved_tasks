"""
żywając interaktywnej powłoki Pythona, utwórz listę przechowującą trzyliterowe skróty
pierwszych sześciu miesięcy roku, a następnie wyświetl tę listę
"""
months = ["St", "Lut", "Marz", "Kw", "Maj", "Czer"]
print([x for x in months])
