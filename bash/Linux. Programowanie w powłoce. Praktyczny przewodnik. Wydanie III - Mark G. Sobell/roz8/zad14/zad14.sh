#!/bin/bash
function basename(){
	tekst="$1"
	echo "${tekst}}" "${2##*.}"
}
basename "$1" "$2"
