#!/bin/bash 
#ssh-keygen -t rsa -b 4096 -C main  -f ansible
# -> generate a key pair
# ssh-copy  -i ansible web1
# -> copy generated key private

#ex1
ansible all -i inventory  -m shell -a 'uptime'   --limit web1  

#ex2
ansible-playbook playbook_ex2.yaml -i inventory  -b 

#ex3
ansible-galaxy collection install community.crypto
ansible-playbook playbook_ex3.yaml -i inventory  -b 

#ex4
ansible-playbook playbook_ex4.yaml -i inventory  -b 

#ex5
ansible-playbook playbook_ex5.yaml -i inventory  -b 

#ex6
ansible-playbook playbook_ex6.yaml -i inventory  -b 

#ex7 
ansible-galaxy collection install community.docker
ansible-playbook playbook_ex7.yaml -i inventory  -b 

#ex8
ansible-playbook playbook_ex8.yaml -i inventory  -b 

#ex9
ansible-playbook playbook_ex9.yaml -i inventory  -b 

#ex11
ansible-playbook playbook_ex11.yaml -i inventory  -b 
