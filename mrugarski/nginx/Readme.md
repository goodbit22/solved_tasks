1. Stwórz trzy serwery webowe, obsługujące kolejno domeny ‘pierwszy.pl’, ‘drugi.pl’
oraz ‘trzeci.pl’. Każda z domen ma odpowiadać innym tekstem (aby dało się je
rozróżnić), a odpytanie domeny nieistniejącej ma wyświetlać zawartość serwera
‘drugi.pl’
Podpowiedź: sites-available + sites-enabled + domyślny vhost (parametr
‘default’)
2. Twoja instancja Nginx ma uruchamiać domyślnie 8 workerów (domyślnie
uruchamia mniej, zazwyczaj tyle, ile masz wątków w procesorze).
Podpowiedź: nginx.conf
3. Spraw, aby katalog o nazwie ‘dane’ w ramach domeny ‘pierwszy.pl’, jeśli nie
zawiera pliku ‘start.html’, był listowany (czyli w przeglądarce wyświetla się spis
znajdujących się w nim plików). Jeśli jednak plik ‘start.html’ istnieje, to ma się
wyświetlić jako domyślny.
Podpowiedź: konfiguracja na poziomie vhosta + dyrektywa ‘index’ + autoindex
na odpowiednie location
4. Pod adresem ‘drugi.pl/test01’ powinny ładować się pliki z katalogu /var/www/abc,
a pod adresem ‘drugi.pl/test02’ pliki z katalogu /var/www/xyz. Dla wszystkich
pozostałych adresów pliki powinny ładować się z katalogu /var/www/domyslne.
Podpowiedź: dyrektywa ‘root’ połączona z odpowiednim location + globalna
definicja ‘root’.
5. Spraw, aby wszystkie pliki *.JPG oraz *.PNG podawane z domeny ‘trzeci.pl’,
posiadały nagłówek cachujący (Expires: ....) ustawiony na 30 dni od daty
wywołania zapytania.
Podpowiedź: location z dopasowaniem do wzorca + expires
6. Skonfiguruj serwer tak, aby wszystkie adresy zaczynające się od słowa ‘skok’ w
domenie ‘drugi.pl’ przekierowywały na stronę mikr.us. Czyli poprawne URLe to
np ‘drugi.pl/skokXY’ lub ‘drugi.pl/skokowy’, czy ‘drugi.pl/skok999’ Podpowiedź:
dyrektywa ‘rewrite’ połączona z prostym wyrażeniem regularnym
7. Spraw, aby odwołanie do nieistniejącego pliku (error 404) skutkowało
wyświetleniem komunikatu o treści ‘Pliku nie ma!’, a do tego aby strona z błędem
miała kod odpowiedzi ‘200 OK’.
Podpowiedź: error_page + odpowiednie trzy parametry
8. Zezwól na wejście na hosta ‘trzeci.pl’ jedynie adresom 1.2.3.4 oraz .9.8.7.6.
Wszystkie pozostałe mają otrzymać komunikat “Zakaz wstępu!”.
Podpowiedź: location + ‘allow ...’ + ‘deny ...’
9. Spraw, aby domena ‘pierwszy.pl’ obsługiwała jedynie zapytania typu GET.
Wszystkie pozostałe mają otrzymać komunikat “Błędna metoda!”
Podpowiedź: dodaj warunek sprawdzający zmienną $request_method. Jeśli nie
jest równa ‘GET’, zwróć dowolny error (np. ‘return 405’) lub użyj konstrukcji
‘limit_except ...’
10. Dodaj do wszystkich plików, których nazwa zaczyna się na literę ‘a’, a do tego
pochodzą z hosta ‘pierwszy.pl’, nagłówek ‘X-Test: OK’
Podpowiedź: add_header + location z odpowiednią maską RegEx
11. Spraw, aby każdemu otwierającemu hosta ‘trzeci.pl’ wyświetlała się zawartość
portalu wp.pl. To nie ma być przekierowanie, a bezpośrednie podanie zawartości.
Dodatkowo podmień wszystkie wystąpienia słowa ‘wczoraj’ na ‘jutro'
Podpowiedź:  location +  proxy_pass + sub_filter